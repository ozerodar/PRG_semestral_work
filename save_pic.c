#include <stdlib.h>
#include <stdio.h>

void save_pic(uint16_t width, uint16_t height, unsigned char *img){
  FILE *fp;
  fp = fopen("output.png", "wb");
  fprintf(fp, "P6\n%d\n%d\n255\n", width,height);
  fwrite(img, 3 * width, height, fp);
  fclose(fp);
}
