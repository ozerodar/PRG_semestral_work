#include "mbed.h"

Serial serial(SERIAL_TX, SERIAL_RX);
InterruptIn button_event(USER_BUTTON);

DigitalOut myled(LED1);
Ticker ticker;

void Tx_interrupt();
void Rx_interrupt();

#define BUF_SIZE 255
#define STARTUP_MSG_LEN 9
#define MESSAGE_SIZE 255
#define VERSION_MAJOR 0
#define VERSION_MINOR 9
#define VERSION_PATCH 0

// Definition of the communication messages
typedef enum {
   MSG_OK,               // ack of the received message
   MSG_ERROR,            // report error on the previously received command
   MSG_ABORT,            // abort - from user button or from serial port
   MSG_DONE,             // report the requested work has been done
   MSG_GET_VERSION,      // request version of the firmware
   MSG_VERSION,          // send version of the firmware as major,minor, patch level, e.g., 1.0p1
   MSG_STARTUP,          // init message (id, up to 9 bytes long string, cksum)
   MSG_SET_COMPUTE,      // set computation parameters
   MSG_COMPUTE_BURST,
   MSG_COMPUTE,          // request computation of a batch of tasks (chunk_id, nbr_tasks)
   MSG_COMPUTE_DATA,     // computed result (chunk_id, result)
   MSG_COMPUTE_DATA_BURST,
   MSG_NBR
} message_type;

typedef struct {
   uint8_t major;
   uint8_t minor;
   uint8_t patch;
} msg_version;

typedef struct {
   uint8_t message[STARTUP_MSG_LEN];
} msg_startup;

typedef struct {
   uint8_t chunk_id;
   uint16_t length; // number of pixels in the data message
   uint8_t *iters;  // pointer to the array of the compute number of iterations
                    // particular values of the array are transmitted; thus,
                    // marshaling and unmarshaling have to be properly handled
                    // together with the proper memory managing
} msg_compute_data_burst;

typedef struct {
   float c_re;  // re (x) part of the c constant in recursive equation
   float c_im;  // im (y) part of the c constant in recursive equation
   float d_re;  // increment in the x-coords
   float d_im;  // increment in the y-coords
   uint8_t n;    // number of iterations per each pixel
} msg_set_compute;

typedef struct {
   uint8_t cid; // chunk id
   float re;    // start of the x-coords (real)
   float im;    // start of the y-coords (imaginary)
   uint16_t n_re; // number of cells in x-coords
   uint16_t n_im; // number of cells in y-coords
} msg_compute;

typedef struct {
   uint8_t cid; // chunk id
   float re;    // start of the x-coords (real)
   float im;    // start of the y-coords (imaginary)
   uint16_t n_re; // number of cells in x-coords
   uint16_t n_im; // number of cells in y-coords
} msg_compute_burst;

typedef struct {
   uint8_t cid;  // chunk id
   uint8_t i_re; // x-coords
   uint8_t i_im; // y-coords
   uint8_t iter; // number of iterations
   uint8_t r; //red
   uint8_t g; // green
   uint8_t b; //blue
} msg_compute_data;

typedef struct {
   uint8_t type;   // message type
   union {
      msg_version version;
      msg_startup startup;
      msg_set_compute set_compute;
      msg_compute compute;
      msg_compute_burst compute_burst;
      msg_compute_data compute_data;
      msg_compute_data_burst compute_data_burst;
   } data;
   uint8_t cksum; // message command
} message;

typedef struct colour colour_t;

struct colour {
  unsigned char red, green, blue;
};

msg_version VERSION = { .major = VERSION_MAJOR, .minor = VERSION_MINOR, .patch = VERSION_PATCH };

volatile bool abort_request = false;
bool burst_computing = false;
bool computing = false;
float period = 1.0;
float compute_time = 0.2;
volatile int height = 240; //default
volatile int width = 320; // default
char tx_buffer[BUF_SIZE];
char rx_buffer[BUF_SIZE];
uint8_t msg_buf[BUF_SIZE];

// pointers to the circular buffers
volatile int tx_in = 0;
volatile int tx_out = 0;
volatile int rx_in = 0;
volatile int rx_out = 0;

void tick()
{
    myled = !myled;
}

void button()
{
    abort_request = true;
}

void Tx_interrupt()
{
    // send a single byte as the interrupt is triggered on empty out buffer
    if (tx_in != tx_out) {
        serial.putc(tx_buffer[tx_out]);
        tx_out = (tx_out + 1) % BUF_SIZE;
    } else { // buffer sent out, disable Tx interrupt
        USART2->CR1 &= ~USART_CR1_TXEIE; // disable Tx interrupt
    }
    return;
}

void Rx_interrupt()
{
    // receive bytes and stop if rx_buffer is full
    while ((serial.readable()) && (((rx_in + 1) % BUF_SIZE) != rx_out)) {
        rx_buffer[rx_in] = serial.getc();
        rx_in = (rx_in + 1) % BUF_SIZE;
    }
    return;
}

bool send_buffer(const uint8_t* msg, unsigned int size)
{
    if (!msg && size == 0) {
        return true;    // size must be > 0
    }
    bool ret = false;
    int i = 0;
    NVIC_DisableIRQ(USART2_IRQn); // start critical section for accessing global data
    bool empty = (tx_in == tx_out);
    while ( (i == 0) || i < size ) { //end reading when message has been read
        if ( ((tx_in + 1) % BUF_SIZE) == tx_out) { // needs buffer space
            NVIC_EnableIRQ(USART2_IRQn); // enable interrupts for sending buffer
            while (((tx_in + 1) % BUF_SIZE) == tx_out) {
                /// let interrupt routine empty the buffer
                serial.attach(&Tx_interrupt, Serial::TxIrq); // attach interrupt handler to transmit data
            }
            NVIC_DisableIRQ(USART2_IRQn); // disable interrupts for accessing global buffer
        }
        tx_buffer[tx_in] = msg[i];
        i += 1;
        tx_in = (tx_in + 1) % BUF_SIZE;
    }
    if (empty && serial.writeable()) {
        uint8_t c = tx_buffer[tx_out];
        tx_out = (tx_out + 1) % BUF_SIZE;
        serial.putc(c); // send first character to start tx interrupts (if stopped)
        serial.attach(&Tx_interrupt, Serial::TxIrq); // attach interrupt handler to transmit data
    }
    NVIC_EnableIRQ(USART2_IRQn); // end critical section
    serial.attach(&Tx_interrupt, Serial::TxIrq); // attach interrupt handler to transmit data
    return ret;
}

// - function  ----------------------------------------------------------------
bool get_message_size(uint8_t msg_type, int *len)
{
   bool ret = true;
   switch(msg_type) {
      case MSG_OK:
      case MSG_ERROR:
      case MSG_ABORT:
      case MSG_DONE:
      case MSG_GET_VERSION:
         *len = 2; // 2 bytes message - id + cksum
         break;
      case MSG_STARTUP:
         *len = 2 + STARTUP_MSG_LEN;
         break;
      case MSG_VERSION:
         *len = 2 + 3 * sizeof(uint8_t); // 2 + major, minor, patch
         break;
      case MSG_SET_COMPUTE:
         *len = 2 + 4 * sizeof(float) + 1; // 2 + 4 * params + n
         break;
      case MSG_COMPUTE:
         *len = 2 + 1 + 2 * sizeof(float) + 2*sizeof(uint16_t); // 2 + cid (8bit) + 2x(double - re, im) + 2 ( n_re, n_im)
         break;
      case MSG_COMPUTE_BURST:
         *len = 2 + 1 + 2 * sizeof(float) + 2*sizeof(uint16_t); // 2 + cid (8bit) + 2x(double - re, im) + 2 ( n_re, n_im)
         break;
      case MSG_COMPUTE_DATA:
         *len = 2 + 6; // cid, dx, dy, r, g, b
         break;
      case MSG_COMPUTE_DATA_BURST:
         *len = 2 + width * height * 3 + 1 * sizeof(uint16_t) + 1;
         break;
      default:
         ret = false;
         break;
   }
   return ret;
}

bool receive_message(uint8_t *msg_buf, int size, int *len)
{
    bool ret = true;
    int i = 0;
    *len = 0; // message size
    NVIC_DisableIRQ(USART2_IRQn); // start critical section for accessing global data
    while ( (i == 0) || (i != *len) ) {
        if (rx_in == rx_out) { // wait if buffer is empty
            NVIC_EnableIRQ(USART2_IRQn); // enable interrupts for receing buffer
            while (rx_in == rx_out) { // wait of next character
            }
            NVIC_DisableIRQ(USART2_IRQn); // disable interrupts for accessing global buffer
        }
        uint8_t c = rx_buffer[rx_out];
        if (i == 0) { // message type
            if (get_message_size(c, len)) { // message type recognized
                msg_buf[i++] = c;
                ret = *len <= size; // msg_buffer must be large enough
            } else {
                ret = false;
                break; // unknown message
            }
        } else {
            msg_buf[i++] = c;
        }
        rx_out = (rx_out + 1) % BUF_SIZE;
    }
    //serial.putc(3);
    NVIC_EnableIRQ(USART2_IRQn); // end critical section
    //if (ret == true) serial.putc(9); else serial.putc(8);
    //if (rx_out == 2) serial.putc(7);
    return ret;
}

// - function  ----------------------------------------------------------------
bool parse_message(const uint8_t *buf, int size, message *msg)
{
   uint8_t cksum = 0;
   for (int i = 0; i < size; ++i) {
      cksum += buf[i];
   }
   bool ret = false;
   int message_size;
   if (
         size > 0 && cksum == 0xff && // sum of all bytes must be 255
         ((msg->type = buf[0]) >= 0) && msg->type < MSG_NBR &&
         get_message_size(msg->type, &message_size) && size == message_size) {
      ret = true;
      switch(msg->type) {
         case MSG_OK:
            break;
         case MSG_ERROR:
            break;
         case MSG_ABORT:
            break;
         case MSG_DONE:
            break;
         case MSG_GET_VERSION:
            break;
         case MSG_STARTUP:
            for (int i = 0; i < STARTUP_MSG_LEN; ++i) {
               msg->data.startup.message[i] = buf[i+1];
            }
            break;
         case MSG_VERSION:
            msg->data.version.major = buf[1];
            msg->data.version.minor = buf[2];
            msg->data.version.patch = buf[3];
            break;
         case MSG_SET_COMPUTE:
            memcpy(&(msg->data.set_compute.c_re), &(buf[1 + 0 * sizeof(float)]), sizeof(float));
            memcpy(&(msg->data.set_compute.c_im), &(buf[1 + 1 * sizeof(float)]), sizeof(float));
            memcpy(&(msg->data.set_compute.d_re), &(buf[1 + 2 * sizeof(float)]), sizeof(float));
            memcpy(&(msg->data.set_compute.d_im), &(buf[1 + 3 * sizeof(float)]), sizeof(float));
            msg->data.set_compute.n = buf[1 + 4 * sizeof(float)];
            break;
         case MSG_COMPUTE: // type + chunk_id + nbr_tasks
            msg->data.compute.cid = buf[1];
            memcpy(&(msg->data.compute.re), &(buf[2 + 0 * sizeof(float)]), sizeof(float));
            memcpy(&(msg->data.compute.im), &(buf[2 + 1 * sizeof(float)]), sizeof(float));
            memcpy(&(msg->data.compute.n_re), &(buf[2 + 2 * sizeof(float)]), sizeof(uint16_t));
            memcpy(&(msg->data.compute.n_im), &(buf[2 + 2 * sizeof(float) + 1 * sizeof(uint16_t)]), sizeof(uint16_t));
            //msg->data.compute.n_re = buf[2 + 2 * sizeof(float) + 0];
            //msg->data.compute.n_im = buf[2 + 2 * sizeof(float) + 2];
            break;
         case MSG_COMPUTE_BURST: // type + chunk_id + nbr_tasks
            msg->data.compute_burst.cid = buf[1];
            memcpy(&(msg->data.compute_burst.re), &(buf[2 + 0 * sizeof(float)]), sizeof(float));
            memcpy(&(msg->data.compute_burst.im), &(buf[2 + 1 * sizeof(float)]), sizeof(float));
            memcpy(&(msg->data.compute_burst.n_re), &(buf[2 + 2 * sizeof(float)]), sizeof(uint16_t));
            memcpy(&(msg->data.compute_burst.n_im), &(buf[2 + 2 * sizeof(float) + 1 * sizeof(uint16_t)]), sizeof(uint16_t));
            //msg->data.compute.n_re = buf[2 + 2 * sizeof(float) + 0];
            //msg->data.compute.n_im = buf[2 + 2 * sizeof(float) + 2];
            break;
         case MSG_COMPUTE_DATA_BURST: // type + chunk_id + nbr_tasks
            msg->data.compute.cid = buf[1];
            memcpy(&(msg->data.compute_burst.re), &(buf[2 + 0 * sizeof(float)]), sizeof(float));
            memcpy(&(msg->data.compute_burst.im), &(buf[2 + 1 * sizeof(float)]), sizeof(float));
            memcpy(&(msg->data.compute_burst.n_re), &(buf[2 + 2 * sizeof(float)]), sizeof(uint16_t));
            memcpy(&(msg->data.compute_burst.n_im), &(buf[2 + 2 * sizeof(float) + 1 * sizeof(uint16_t)]), sizeof(uint16_t));
            //msg->data.compute.n_re = buf[2 + 2 * sizeof(float) + 0];
            //msg->data.compute.n_im = buf[2 + 2 * sizeof(float) + 2];
            break;
         case MSG_COMPUTE_DATA:  // type + chunk_id + task_id + result
            msg->data.compute_data.cid = buf[1];
            msg->data.compute_data.i_re = buf[2];
            msg->data.compute_data.i_im = buf[3];
            msg->data.compute_data.r = buf[4];
            msg->data.compute_data.g = buf[5];
            msg->data.compute_data.b = buf[6];
            break;
         default: // unknown message type
            ret = false;
            break;
      } // end switch
   }
   return ret;
}

bool send_message(const message *msg, uint8_t *buf, int size)
{
    if (!msg || size < sizeof(message) || !buf) {
        return false;
    }
    // 1st - serialize the message into a buffer
    bool ret = true;
    int len = 0;
    switch(msg->type) {
        case MSG_OK:
            len = 1;
            break;
        case MSG_ERROR:
            len = 1;
            break;
        case MSG_ABORT:
            len = 1;
            break;
        case MSG_DONE:
            len = 1;
            break;
        case MSG_GET_VERSION:
            len = 1;
            break;
        case MSG_STARTUP:
            for (int i = 0; i < STARTUP_MSG_LEN; ++i) {
                buf[i+1] = msg->data.startup.message[i];
            }
            len = 1 + STARTUP_MSG_LEN;
            break;
        case MSG_VERSION:
            buf[1] = msg->data.version.major;
            buf[2] = msg->data.version.minor;
            buf[3] = msg->data.version.patch;
            len = 4;
            break;
        case MSG_SET_COMPUTE:
            memcpy(&(buf[1 + 0 * sizeof(float)]), &(msg->data.set_compute.c_re), sizeof(float));
            memcpy(&(buf[1 + 1 * sizeof(float)]), &(msg->data.set_compute.c_im), sizeof(float));
            memcpy(&(buf[1 + 2 * sizeof(float)]), &(msg->data.set_compute.d_re), sizeof(float));
            memcpy(&(buf[1 + 3 * sizeof(float)]), &(msg->data.set_compute.d_im), sizeof(float));
            buf[1 + 4 * sizeof(float)] = msg->data.set_compute.n;
            len = 1 + 4 * sizeof(float) + 1;      // lo - nbr_tasks
            break;
        case MSG_COMPUTE:
            buf[1] = msg->data.compute.cid; // cid
            memcpy(&(buf[2 + 0 * sizeof(float)]), &(msg->data.compute.re), sizeof(float));
            memcpy(&(buf[2 + 1 * sizeof(float)]), &(msg->data.compute.im), sizeof(float));
            memcpy(&(buf[2 + 2 * sizeof(float)]), &(msg->data.compute.n_re), sizeof(uint16_t));
            memcpy(&(buf[2 + 2 * sizeof(float) + 1 * sizeof(uint16_t)]), &(msg->data.compute.n_im), sizeof(uint16_t));
            //buf[2 + 2 * sizeof(float) + 0] = msg->data.compute.n_re;
          //  buf[2 + 2 * sizeof(float) + 2] = msg->data.compute.n_im;
            len = 1 + 1 + 2 * sizeof(float) + 4;
            break;
        case MSG_COMPUTE_DATA:
            buf[1] = msg->data.compute_data.cid;
            buf[2] = msg->data.compute_data.i_re;
            buf[3] = msg->data.compute_data.i_im;
            buf[4] = msg->data.compute_data.r;
            buf[5] = msg->data.compute_data.g;
            buf[6] = msg->data.compute_data.b;
            len = 7;
            break;
        case MSG_COMPUTE_DATA_BURST:
            len = width * height * 3 + 1 * sizeof(uint16_t) + 1;
            buf[1] = msg->data.compute_data_burst.chunk_id;
            memcpy(&(buf[2]), &(msg->data.compute_data_burst.length), sizeof(uint16_t));
            for (int i = 0; i < width * height * 3; i++) {
              memcpy(&(buf[2 + 1 * sizeof(uint16_t) + i * sizeof(uint8_t)]), &(msg->data.compute_data_burst.iters[i]), sizeof(uint8_t));
            }
            //free(msg->data.compute_data_burst.iters);
            break;
        default: // unknown message type
            ret = false;
            break;
    }
    // 2nd - send the message buffer
    if (ret) { // message recognized
        buf[0] = msg->type;
        buf[len] = 0; // cksum
        for (int i = 0; i < len; ++i) {
            buf[len] += buf[i];
        }
        buf[len] = 255 - buf[len]; // compute cksum
        len += 1; // add cksum to buffer
        ret = send_buffer(buf, len);
    }
    return ret;
}

int calculate_color(float zi_Re, float zi_Im, int n, float c_Re, float c_Im) {
  int i = 0;
  float old_Re;
  float old_Im;
  float qv_Re;
  float qv_Im;
  float two_old_Re_Im;

  while (i != n) {
    old_Re = zi_Re;
    old_Im = zi_Im;

    qv_Re = old_Re * old_Re; // the formula for the (a + b)^2
    qv_Im = old_Im * old_Im;
    two_old_Re_Im = 2 * old_Re * old_Im;

    zi_Re = qv_Re - qv_Im + c_Re;
    zi_Im = two_old_Re_Im + c_Im;

    if ((zi_Re * zi_Re + zi_Im * zi_Im) >= 4.0) {
      break;
    }
    i++;
  }
  return i;
}

colour_t get_RGB(int k, int n){
  colour_t colour;
  float red, green, blue;
  float t = (float)k/(float)n;
  red = 9 * (1 - t) * t * t * t * 255;
  green = 15 * (1 - t) * (1 - t) * t * t * 255;
  blue =  8.5 * (1 - t) * (1 - t) * (1 - t) * t * 255;

  colour.red = (unsigned char)red;
  colour.green = (unsigned char)green;
  colour.blue = (unsigned char)blue;
  return colour;
}

int main(void) {

  serial.baud(115200);
  message msg;
  msg_compute compute_data;
  button_event.rise(&button);
  colour_t colour;
  int iterations = 10; //default
  float re_int = 3.2; //default
  float im_int = 2.2; //default
  float a_start = -1.6; //default
  float b_start = -1.1; // default
  float c_Re = -0.4; //default
  float c_Im = 0.6; //default
  float a = re_int/width;
  float b = im_int/height;
  int k = 1;
  int x = 0;
  int y = 0;
  //int m = 0;
  unsigned char color;

    for (int i = 0; i < 5*2; ++i) { // 5x zablikání LED s periodou 50 ms
        myled = !myled;
        wait(0.05);
    }

    message m = { .data.startup.message =  { 'O', 'Z', 'E', 'R', 'O', '-', 'D', 'A', 'R' } };

    m.type = MSG_STARTUP;
    send_message(&m, msg_buf, MESSAGE_SIZE);

    serial.attach(&Rx_interrupt, Serial::RxIrq); // attach interrupt handler to receive data
    serial.attach(&Tx_interrupt, Serial::TxIrq); // attach interrupt handler to transmit data

     while (1) {
        if (abort_request) {
         if (computing) {  //abort computing
            msg.type = MSG_ABORT;
            send_message(&msg, msg_buf, MESSAGE_SIZE);
            computing = false;
            abort_request = false;
            ticker.detach();
            myled = 0;
         }
      }
        if (rx_in != rx_out) { // something is in the receive buffer
            int msg_len = 0;
            if (receive_message(msg_buf, MESSAGE_SIZE, &msg_len)) {
                if (parse_message(msg_buf, msg_len, &msg)) {
                    switch(msg.type) {
                        case MSG_GET_VERSION:
                            msg.type = MSG_VERSION;
                            msg.data.version  = VERSION;
                            send_message(&msg, msg_buf, MESSAGE_SIZE);
                            break;
                        case MSG_ABORT:
                            msg.type = MSG_OK;
                            send_message(&msg, msg_buf, MESSAGE_SIZE);

                            if (computing == true){
                                msg.type = MSG_ABORT;
                                send_message(&msg, msg_buf, MESSAGE_SIZE);
                                computing = false;
                            }
                            //abort_request = false;
                            ticker.detach();
                            myled = 0;
                            break;
                        case MSG_COMPUTE:
                            if (msg.data.compute.n_re > 0) {
                                ticker.attach(tick, period);
                                compute_data.cid = msg.data.compute.cid;
                                compute_data.n_re = msg.data.compute.n_re;
                                compute_data.n_im = msg.data.compute.n_im;
                                compute_data.re = msg.data.compute.re;
                                compute_data.im = msg.data.compute.im;
                                if (y == (height)) {
                                    y = 0;
                                }
                                //a_start = compute_data.re;
                                //b_start = compute_data.im;
                                height = msg.data.compute.n_im;
                                width = msg.data.compute.n_re;
                                a = re_int/width;
                                b = im_int/height;
                                    //compute_data.task_id = 0; // reset the task counter
                                computing = true;
                                msg.type = MSG_OK;
                                send_message(&msg, msg_buf, MESSAGE_SIZE);
                              }
                              break;
                        case MSG_COMPUTE_BURST:
                            if (msg.data.compute_burst.n_re > 0) {
                                //ticker.attach(tick, period);
                                compute_data.cid = msg.data.compute_burst.cid;
                                compute_data.n_re = msg.data.compute_burst.n_re;
                                compute_data.n_im = msg.data.compute_burst.n_im;
                                compute_data.re = msg.data.compute_burst.re;
                                compute_data.im = msg.data.compute_burst.im;
                                if (y == (height)) {
                                     y = 0;
                                }
                                height = msg.data.compute_burst.n_im;
                                width = msg.data.compute_burst.n_re;
                                a = re_int/width;
                                b = im_int/height;
                                burst_computing = true;
                                msg.type = MSG_OK;
                                send_message(&msg, msg_buf, MESSAGE_SIZE);
                              }
                              break;
                          case MSG_SET_COMPUTE:
                              c_Re = msg.data.set_compute.c_re;  // re (x) part of the c constant in recursive equation
                              c_Im = msg.data.set_compute.c_im;  // im (x) part of the c constant in recursive equation
                              a_start = msg.data.set_compute.d_re;
                              b_start = msg.data.set_compute.d_im;
                              re_int = a_start * 2;
                              im_int = b_start * 2;
                              if (re_int < 0) {
                                  re_int *= -1;
                              }
                              if (im_int < 0) {
                                  im_int *= -1;
                              }
                              a = re_int/width;
                              b = im_int/height;
                              iterations = msg.data.set_compute.n;    // number of iterations per each pixel
                              msg.type = MSG_OK;
                              send_message(&msg, msg_buf, MESSAGE_SIZE);
                              y = 0;
                              break;
                    } // end switch
                } else { // message has not been parsed send error
                    msg.type = MSG_ERROR;
                    send_message(&msg, msg_buf, MESSAGE_SIZE);
                }
            } // end message received
        } else if (computing) {
            if (y < height) {
              for (int x = 0; x < width; x++) {
                  k = calculate_color(a_start + ((x) * a), b_start + ((y) * b), iterations, c_Re, c_Im);
                  colour = get_RGB(k, iterations);

                  //wait(compute_time); // do some computation
                  msg.type = MSG_COMPUTE_DATA;
                  msg.data.compute_data.cid = compute_data.cid;
                  msg.data.compute_data.i_re = x;
                  msg.data.compute_data.i_im = y;
                  msg.data.compute_data.r = colour.red;
                  msg.data.compute_data.g = colour.green;
                  msg.data.compute_data.b = colour.blue;
                  send_message(&msg, msg_buf, MESSAGE_SIZE);
                  //compute_data.task_id += 1;
                }
                y++;
            } else { //computation done
                ticker.detach();
                myled = 0;
                msg.type = MSG_DONE;
                send_message(&msg, msg_buf, MESSAGE_SIZE);
                computing = false;
            }
        } else if (burst_computing) {
            msg.type = MSG_COMPUTE_DATA_BURST;
            msg.data.compute_data_burst.chunk_id = compute_data.cid;
            msg.data.compute_data_burst.length = width * height * 3;
            msg.data.compute_data_burst.iters = (uint8_t*)malloc(width * height * 3 * sizeof(uint8_t));
            for (int i = 0; i < width * height * 3; i++) {
              msg.data.compute_data_burst.iters[i] = 0;
            }
            /*while (y < height) {
              for (int x = 0; x < width; x++) {
                  k = calculate_color(a_start + ((x) * a), b_start + ((y) * b), iterations, c_Re, c_Im);
                  colour = get_RGB(k, iterations);

                  for (int m = 0; m < 3; m++) {
                    if (m == 0) { // red
                      msg.data.compute_data_burst.iters[y * width * 3 + x * 3 + m] = colour.red;
                    }
                    if (m == 1) { // green
                      msg.data.compute_data_burst.iters[y * width * 3 + x * 3 + m] = colour.green;
                    }
                    if (m == 2) { // blue
                      msg.data.compute_data_burst.iters[y * width * 3 + x * 3 + m] = colour.blue;
                    }
                  }
                  //compute_data.task_id += 1;
                }
                y++;
              }*/
              //computation done
              ticker.detach();
              send_message(&msg, msg_buf, MESSAGE_SIZE);
              myled = 0;
              burst_computing = false;
              free(msg.data.compute_data_burst.iters);
        }
        else {
            sleep(); // put the cpu to sleep mode, it will be wakeup on interrupt
        }
    } // end while (1)
}
