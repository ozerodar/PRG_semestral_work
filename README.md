Projekt se zkompiluje pripravenym Makefile prikazem "make", spousti se "./prgsem-main".

Zakladni vlastnosti:
1. Vysledek vypoctu Juliovy mnoziny je prenasen po pixelech a prubezne zobrazovan v grafickem okne aplikace.
2. Program lze spustit bez argumentu. V tom pripade mnozina se pocita s default parametry a velikost obrazku zadava uzivatel hned na zacatku. Pokud program je spousten s argumenty 1. argument definuje konkretni port, 2. a 3. velikost obrazku a 4. počet iterací
3. Beh programu doplnuji Kontrolni hlasky o stavu aplikace, ktere popisuji co se v aplikaci deje
4. Probíhající výpočet na Nucleo desce lze kdykoliv prerusit z ridici aplikace nebo stisknutim uzivatelskeho tlacitka na Nucleo desce.
5. zakladni funkcionality (zkratky):
'g' - verze
's' - nastavi parametry vypoctu (konstantu c a zoom)
'1' - spusti vypocet
'a' - prerusi probihajici vypocet
'r' - resetuje cid
'l' - smaze aktualni obsah vypoctu
'p' - prekresli obsah okna aktualnim stavem vypoctu
'c' - spocte fraktal na PC
'q' - ukonci program

Extra funkcionality:
1. Interaktivni volba velikosti obrazku.
Pokud uzivatel nezada argumenty, program naskenuje hodnoty sirsky a vysky hned na zacatku programu.
2. Interaktivni volba parametru c
3. Animace obrazku fraktalu ktery je realizovan v ramci ovladaci aplikace na PC.
4. zrychleny vypocet fraktalu pomoci zpravy s nekolika pixely
4. zkratky:
'b' - zapne zrychleny vypocet
'k' - precte a nastavi parametr c
'3' - zpusti animaci
'z' - zastavi animaci

Bonusove funkcionality:
1. Lze ulozit obrazek do souboru .png v libovolnou chvili pomoci zkratky '4'.

Upozornim na to, ze program obsahuje vlakno "redraw", ktere pravidelne vola funkci vyprazdnujici frontu zprav aplikace od grafickeho rozhrani. Odevzdana prace je udelana aby se sla hezky spustit na Linux, ale pri ukazce ja budu pouzivat MacOS, to ztmavnuti tam neni a nelibi se mu ze se tam neco takoveho deje. Proto nektere veci tam zakomentuji.
