#include <stdio.h>
#include <stdlib.h>

#include "xwin_sdl.h"

typedef struct colour colour_t;

struct colour {
  unsigned char red, green, blue;
};

void print_complex(float i, float j) {
  printf("%f + %fi\n", i,j);
}

int calculate_color(float zi_Re, float zi_Im, int n) {
  int i = 0;
  float c_Re = -0.4;
  float c_Im = 0.6;
  float old_Re;
  float old_Im;
  float qv_Re;
  float qv_Im;
  float two_old_Re_Im;

  while (i != n) {
    old_Re = zi_Re;
    old_Im = zi_Im;

    qv_Re = old_Re * old_Re; // the formula for the (a + b)^2
    qv_Im = old_Im * old_Im;
    two_old_Re_Im = 2 * old_Re * old_Im;

    zi_Re = qv_Re - qv_Im + c_Re;
    zi_Im = two_old_Re_Im + c_Im;

    if ((zi_Re * zi_Re + zi_Im * zi_Im) >= 4.0) {
      break;
    }
    i++;
  }
  return i;
}

colour_t get_RGB(int k, int n){
  colour_t colour;
  float red, green, blue;
  float t = (float)k/(float)n;
  red = 9 * (1 - t) * t * t * t * 255;
  green = 15 * (1 - t) * (1 - t) * t * t * 255;
  blue =  8.5 * (1 - t) * (1 - t) * (1 - t) * t * 255;

  colour.red = (unsigned char)red;
  colour.green = (unsigned char)green;
  colour.blue = (unsigned char)blue;
  return colour;
}

int main(int argc, char const *argv[]) {
  colour_t colour;
  int iterations = 200;
  int height = 480;
  int width = 640;
  float re_int = 3.2;
  float im_int = 2.2;
  float a = re_int/width;
  float b = im_int/height;
  int k = 1;
  char c;

  unsigned char *img = (unsigned char*)malloc(width * height * sizeof(unsigned char***));

  for (int y = 0; y < height; y++) {
    for (int x = 0; x < width; x++) {
      for (int m = 0; m < 3; m++) {
        k = calculate_color(-1.6 + ((x) * a), -1.1 + ((y) * b), iterations);
        colour = get_RGB(k, iterations);
        if (m == 0) { // red
          img[y * width * 3 + x * 3 + m] = colour.red;
        }

        if (m == 1) { // green
          img[y * width * 3 + x * 3 + m] = colour.green;
        }
        if (m == 2) { // blue
          img[y * width * 3 + x * 3 + m] = colour.blue;
        }
      }
    }
  }

  xwin_init(width, height);
  xwin_redraw(width, height, img);
  while (1) {
    if (scanf("%c", &c)) {
      if (c == 'q') {
        break;
      }
    }
  }
  xwin_close();
  free(img);
  return 0;
}
