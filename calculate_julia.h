#include "xwin_sdl.h"
#include "png.h"


typedef struct colour colour_t;

struct colour {
  unsigned char red, green, blue;
};

int calculate_color(float zi_Re, float zi_Im, int n);
colour_t get_RGB(int k, int n);
void set_up_julia();
void calculate_julia();
void* display_animation(void *d);
