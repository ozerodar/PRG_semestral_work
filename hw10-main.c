#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>

#include <assert.h>

#include <termios.h>
#include <unistd.h>  // for STDIN_FILENO

#include <pthread.h>

#include "prg_serial_nonblock.h"
#include "messages.h"
#include "event_queue.h"
#include "xwin_sdl.h"
#include "calculate_julia.h"

#define SERIAL_READ_TIMOUT_MS 500 //timeout for reading from serial port

// shared data structure
typedef struct {
   bool quit;
   int fd; // serial port file descriptor
} data_t;

pthread_mutex_t mtx;
pthread_cond_t cond;

void call_termios(int reset);
void* input_thread(void*);
void* serial_rx_thread(void*); // serial receive buffer
void* reload_thread(void*);
void fill_graph(unsigned char r, unsigned char g, unsigned char b, int y, int x);
bool send_message(data_t *data, const message *msg);

message msg;
int quit_ctr = 0;
int chunk_id = 0;

volatile uint16_t width = 320; // default
volatile uint16_t height = 240; // default
volatile int iterations = 60; // default



volatile float a_start = -1.6;
volatile float b_start = -1.1;


float c_Re = 0.1; // default
float c_Im = -0.6; //default
int x = 0;
int y = 0;
unsigned char *img;
bool end = false;
bool end_a = false;
bool quit = false;
bool computing = false;
bool next_x = false;
bool next_y = false;
bool flag = false;
bool reset = false;

// - main ---------------------------------------------------------------------
int main(int argc, char *argv[])
{
  //int ser = 115200;

  if (argc > 2) {
    width = atoi(argv[2]);
    if (argc > 3) {
      height = atoi(argv[3]);
      if (argc > 4) {
        iterations = atoi(argv[4]);
      }
    }
  }

  //float d_re = 3.2;  // increment in the x-coords
  //float d_im = 2.2;  // increment in the y-coords
  img = (unsigned char*)calloc(width * height, 1 * sizeof(unsigned char***));
   data_t data = { .quit = false, .fd = -1};
   const char *serial = argc > 1 ? argv[1] : "/dev/cu.usbmodem1423";
   data.fd = serial_open(serial);
   int idx = 0;
   int c;

   printf("Width of the image:\n**(recommended: 320)**\n");
   scanf("%d", &c);
   if (width != c) {
     while (c >= 700) {
       fprintf(stderr, "WARN: The width is too big\n");
       scanf("%d", &c);
     }
     width = c;
   }
   printf("Height of the image:\n**(recommended: 240)**\n");
   scanf("%d", &c);
   if (height != c) {
     while (c >= 700) {
       fprintf(stderr, "ERROR: The height is too big\n");
       scanf("%d", &c);
     }
     height = c;
   }

   xwin_init(width, height);

   if (data.fd == -1) {
      fprintf(stderr, "ERROR: Cannot open serial port %s\n", serial);
      exit(100);
   }

  enum { INPUT, SERIAL_RX, NUM_THREADS };
   //enum { INPUT, SERIAL_RX, REDRAW, NUM_THREADS };
  const char *threads_names[] = { "Input", "Serial In"};
  //const char *threads_names[] = { "Input", "Serial In, Redraw"};
  void* (*thr_functions[])(void*) = { input_thread, serial_rx_thread};
  // void* (*thr_functions[])(void*) = { input_thread, serial_rx_thread, reload_thread};

   pthread_t threads[NUM_THREADS];
   pthread_mutex_init(&mtx, NULL); // initialize mutex with default attributes
   pthread_cond_init(&cond, NULL); // initialize mutex with default attributes

   call_termios(0);

   for (int i = 0; i < NUM_THREADS; ++i) {
      int r = pthread_create(&threads[i], NULL, thr_functions[i], &data);
      fprintf(stderr, "INFO: Create thread '%s' %s\n", threads_names[i], ( r == 0 ? "OK" : "FAIL") );
   }

   // example of local variables for computation and messaging

   while (!quit) {
      if (quit_ctr == NUM_THREADS) {
        quit = true;
      }
      //example of the event queue
      event ev = queue_pop();
      if (ev.source == EV_KEYBOARD) {
         msg.type = MSG_NBR;
         // handle keyboard events
         switch(ev.type) {
            case EV_COMPUTE:
                msg.type = MSG_COMPUTE;
                msg.data.compute.cid = chunk_id;
                msg.data.compute.re = a_start;
                msg.data.compute.im = b_start;
                msg.data.compute.n_re = width;
                msg.data.compute.n_im = height;
                chunk_id++;
                computing = true;
                fprintf(stderr, "INFO: New computation chunk id: %d for part %d x %d\n", msg.data.compute.cid, msg.data.compute.n_re, msg.data.compute.n_im);
                break;
            case EV_SET_COMPUTE:
                msg.type = MSG_SET_COMPUTE;
                msg.data.set_compute.c_re = c_Re;
                msg.data.set_compute.c_im = c_Im;
                msg.data.set_compute.d_re = a_start;
                msg.data.set_compute.d_im = b_start;
                msg.data.set_compute.n = iterations;
                idx = 0;
                //printf("iter: %d, n: %d\n",iterations, msg.data.set_compute.n);
                fprintf(stderr, "INFO: Set new computation resolution. c = %1f + %1fi. x coord starts from %2f, y coord starts from %2f\n", msg.data.set_compute.c_re, msg.data.set_compute.c_im, msg.data.set_compute.d_re,
                 msg.data.set_compute.d_im);
                break;
            case EV_ABORT:
                msg.type = MSG_ABORT;
                if (!computing) {
                  fprintf(stderr, "WARN: Abort requested but it is not computing\n");
                }
                fprintf(stderr, "INFO: Abort request was sent\n");
                break;
            case EV_GET_VERSION:
               { // prepare packet for get version
                  msg.type = MSG_GET_VERSION;
                  fprintf(stderr, "INFO: Get version requested\n");
               }
               break;
            case EV_QUIT:
                quit = true;
            default:
               break;
         }
         if (msg.type != MSG_NBR) { // messge has been set
            if (!send_message(&data, &msg)) {
               fprintf(stderr, "ERROR: send_message() does not send all bytes of the message!\n");
            }
         }
      } else if (ev.source == EV_NUCLEO) { // handle nucleo events
         if (ev.type == EV_SERIAL) {
            message *msg = ev.data.msg;
            switch (msg->type) {
              case MSG_ERROR:
                  fprintf(stderr, "ERROR: Receive error from Nucleo\n");
                  break;
              case MSG_OK:
                  fprintf(stderr, "INFO: Receive ok from Nucleo\n");
                  break;
               case MSG_STARTUP:
                  {
                     char str[STARTUP_MSG_LEN+1];
                     for (int i = 0; i < STARTUP_MSG_LEN; ++i) {
                        str[i] = msg->data.startup.message[i];
                     }
                     str[STARTUP_MSG_LEN] = '\0';
                     fprintf(stderr, "INFO: Nucleo restarted - '%s'\n", str);
                  }
                  break;
               case MSG_VERSION:
                  if (msg->data.version.patch > 0) {
                     fprintf(stderr, "INFO: Nucleo firmware ver. %d.%d-p%d\n", msg->data.version.major, msg->data.version.minor, msg->data.version.patch);
                  } else {
                     fprintf(stderr, "INFO: Nucleo firmware ver. %d.%d\n", msg->data.version.major, msg->data.version.minor);
                  }
                  break;
              case MSG_COMPUTE_DATA:
                  if (flag == true) {
                    fprintf(stderr, "INFO: New data for x = %d, y = %d, r: %d, g: %d, b: %d\n", msg->data.compute_data.i_re + 255, msg->data.compute_data.i_im,
                    msg->data.compute_data.r, msg->data.compute_data.g, msg->data.compute_data.b);
                  } else {
                    fprintf(stderr, "INFO: New data for x = %d, y = %d, r: %d, g: %d, b: %d\n", msg->data.compute_data.i_re, msg->data.compute_data.i_im,
                    msg->data.compute_data.r, msg->data.compute_data.g, msg->data.compute_data.b);
                  }
                  if (msg->data.compute_data.i_re == 255) {
                      flag = true;
                  } else if (idx % width == 0) {
                      flag = false;
                  }
                  img[idx++] = msg->data.compute_data.r;
                  img[idx++] = msg->data.compute_data.g;
                  img[idx++] = msg->data.compute_data.b;
                  fill_graph(msg->data.compute_data.r, msg->data.compute_data.g, msg->data.compute_data.b, msg->data.compute_data.i_im, msg->data.compute_data.i_re);
                  break;
              case MSG_DONE:
                  computing = false;
                  fprintf(stderr, "INFO: Nucleo reports the computation is done computing: 1\n");
                  break;
               default:
                  break;
            }
            if (msg) {
               free(msg);
            }
         } else if (ev.type == EV_QUIT) {
            quit = true;
         } else {
            // ignore all other events
         }
      }

   } // end main quit
   queue_cleanup(); // cleanup all events and free allocated memory for messages.
   for (int i = 0; i < NUM_THREADS; ++i) {
      fprintf(stderr, "INFO: Call join to the thread %s\n", threads_names[i]);
      int r = pthread_join(threads[i], NULL);
      fprintf(stderr, "INFO: Joining the thread %s has been %s\n", threads_names[i], (r == 0 ? "OK" : "FAIL"));
   }
   serial_close(data.fd);
   call_termios(1); // restore terminal settings
   xwin_close();
   free(img);
   return EXIT_SUCCESS;
}


void fill_graph(unsigned char r, unsigned char g, unsigned char b, int y, int x) {
  //if (next_x) {
  //  x += 255;
  //}
  //img[y * width * 3 + x * 3 + 0] = r;
  //img[y * width * 3 + x * 3 + 1] = g;
  //img[y * width * 3 + x * 3 + 2] = b;
  if (x == 0) {
    xwin_redraw(width, height, img);
  }
}



// - function -----------------------------------------------------------------
void call_termios(int reset)
{
   static struct termios tio, tioOld;
   tcgetattr(STDIN_FILENO, &tio);
   if (reset) {
      tcsetattr(STDIN_FILENO, TCSANOW, &tioOld);
   } else {
      tioOld = tio; //backup
      cfmakeraw(&tio);
      tio.c_oflag |= OPOST;
      tcsetattr(STDIN_FILENO, TCSANOW, &tio);
   }
}

void* reload_thread(void* d) {
  while (!end) {
    xwin_poll_events();
    sleep(1);
  }
  fprintf(stderr, "INFO: Exit redraw thead %p\n", (void*)pthread_self());
  quit_ctr++;
  return NULL;
}

// - function -----------------------------------------------------------------
void* input_thread(void* d)
{
   data_t *data = (data_t*)d;
   data_t data2 = { .quit = false};
   pthread_t animation;
   int c;
     event ev = { .source = EV_KEYBOARD };
   while ( !end && (c = getchar())) {
           ev.type = EV_TYPE_NUM;
      if (c == 'g') {
        ev.type = EV_GET_VERSION;
      } else if (c == '1') {
        ev.type = EV_COMPUTE;
      } else if (c == 'a') {
        ev.type = EV_ABORT;
        computing = false;
      } else if (c == 's') {
        if (computing == false) {
          call_termios(1); // restore terminal settings
          ev.type = EV_SET_COMPUTE;
          printf("Type the real part of a complex constant:\n**(recommended: -0.4)**\n");
          scanf("%f", &c_Re);
          printf("Type the imaginary part of a complex constant:\n**(recommended: 0.6)**\n");
          scanf("%f", &c_Im);
          printf("Type the real part of the start point:\n**(recommended: -1.6 or -0.5)**\n");
          scanf("%f", &a_start);
          printf("Type the imaginary part of the start point:\n**(recommended: -1.1 or -0.5)**\n");
          scanf("%f", &b_start);
          printf("Type the number of iterations:\n**(recommended: 60)**\n");
          scanf("%d", &iterations);
          call_termios(0); // restore terminal settings
        } else {
          fprintf(stderr, "WARN: Settings are not possible, it is currently computing\n");
        }
      } else if (c == 'q') {
        if (computing) {
          msg.type = MSG_ABORT;
          if (!send_message(data, &msg)) {
             fprintf(stderr, "ERROR: send_message() does not send all bytes of the message!\n");
          } else {
            fprintf(stderr, "INFO: Quit request was sent\n");
          }
        }
        end = true;
        ev.type = EV_QUIT;
      } else if (c == 'r') {
        if (computing == false) {
          fprintf(stderr, "INFO: Chunk reset request\n");
          chunk_id = 0;
        } else {
          fprintf(stderr, "WARN: Chunk reset request discarded, it is currently computing\n");
        }
      } else if (c == 'i') {
        for (int i = 0; i < width * height * 3; i++) {
          img[i] = 0;
        }
        xwin_redraw(width, height, img);
        fprintf(stderr, "INFO: The image was cleaned\n");
      } else if (c == 'p') {
        fprintf(stderr, "INFO: Forced redrawing by the actual computations\n");
        xwin_redraw(width, height, img);
      } else if (c == 'c') {
        set_up_julia(img, width, height, iterations, a_start, b_start);
        calculate_julia();
        //xwin_redraw(width, height, img);
      } else if (c == 'k') {
        if (computing == false) {
          call_termios(1); // restore terminal settings
          ev.type = EV_SET_COMPUTE;
          printf("Type the real part of a complex constant:\n**(recommended: -0.4)**\n");
          scanf("%f", &c_Re);
          printf("Type the imaginary part of a complex constant:\n**(recommended: 0.6)**\n");
          scanf("%f", &c_Im);
          call_termios(0); // restore terminal settings
        } else {
          fprintf(stderr, "WARN: Settings are not possible, it is currently computing\n");
        }
      } else if (c == '2') {
        fprintf(stderr, "INFO: Animated mode is on. Press 'z' to stop\n");
        pthread_create(&animation, NULL, display_animation, &data2);
        //data2.quit = true;
        fprintf(stderr, "INFO: Call join to the thread display_animation\n");
        //fprintf(stderr, "INFO: Create thread '%s' %s\n", threads_names[i], ( r == 0 ? "OK" : "FAIL") );
        //calculate_julia(img, width, height, iterations, a_start, b_start, true);
        //xwin_redraw(width, height, img);
      } else if (c == 'z') {
        data2.quit = true;
        int r = pthread_join(animation, NULL);
        fprintf(stderr, "INFO: Joining the thread display_animation has been %s\n", (r == 0 ? "OK" : "FAIL"));
      }
          if (ev.type != EV_TYPE_NUM) { // new event
             queue_push(ev);
          }
      pthread_mutex_lock(&mtx);
      end = end || data->quit; // check for quit
      pthread_mutex_unlock(&mtx);
   }
   // ev.type = EV_QUIT;
   // queue_push(ev);
   fprintf(stderr, "INFO: Exit input thead %p\n", (void*)pthread_self());
   quit_ctr++;
   return NULL;
}

// - function -----------------------------------------------------------------
void* serial_rx_thread(void* d)
{ // read bytes from the serial and puts the parsed message to the queue
   data_t *data = (data_t*)d;
    uint8_t msg_buf[sizeof(message)]; // maximal buffer for all possible messages defined in messages.h
    event ev = { .source = EV_NUCLEO, .type = EV_SERIAL, .data.msg = NULL };
    int i = 0;
    int len = 0;
   unsigned char c;
   while (serial_getc_timeout(data->fd, SERIAL_READ_TIMOUT_MS, &c) > 0) {
     if (end == true) {
       break;
     }
   }; // discard garbage

   while (!end) {
      int r = serial_getc_timeout(data->fd, SERIAL_READ_TIMOUT_MS, &c);
      if (r > 0) { // character has been read
         //printf("received %d\n", c);
         msg_buf[i] = c;
         if (i == 0) {
           if (!get_message_size(c, &len)) {
             fprintf(stderr, "ERROR: Cannot calculate the size of the message\n");
           } else {
             ev.data.msg = (message*)malloc(sizeof(message) * len);
           }
         }
         if (i == len - 1) {
           //for (int h = 0; h < len; h++) {
             //printf("buff: %d\n", msg_buf[h]);
           //}
           if (!parse_message_buf(msg_buf, len, ev.data.msg)) {
             fprintf(stderr, "ERROR: Cannot parse the message\n");
           } else {
             queue_push(ev);
             len = 0;
             i = 0;
           }
         } else {
           i++;
         }
      } else if (r == 0) { //read but nothing has been received
        // if (c_one & !computing) {
        //   ev.type = EV_COMPUTE;
        //   msg.data.compute.cid = chunk_id;
        //   chunk_id++;
        //
        //   msg.data.compute.re = 0;
        //   msg.data.compute.im = y;
        //   msg.data.compute.n_re = width;
        //   msg.data.compute.n_im = 1;
        // }
      } else {
         fprintf(stderr, "ERROR: Cannot receive data from the serial port\n");
         end = true;
      }
   }
   // ev.type = EV_QUIT;
   // queue_push(ev);
   fprintf(stderr, "INFO: Exit serial_rx_thread %p\n", (void*)pthread_self());
   quit_ctr++;
   return NULL;
}

// - function -----------------------------------------------------------------
bool send_message(data_t *data, const message *msg)
{
  //int len = 0;
  //printf("%d\n", data->fd);
  int buff_size = 0;
  //    printf("%d\n", msg->type);
//  printf("!\n");

  //printf("number of iterations: %d\n", msg->data.set_compute.n);
  uint8_t buf[get_message_size(msg->type, &buff_size)];

  if (fill_message_buf(msg, buf, 100, &buff_size) == false) {
      fprintf(stderr, "ERROR: Cannot fill the message buffer\n");
      return false;
  }

      //printf("type %d\n", buf[0]);

//  for (int i = 0; i < buff_size; i++) {
  //  printf("%d\n", buf[i]);
//  }

  for (int i = 0; i < buff_size; i++) {
  //  printf("%d\n", i);

      //printf("");

    serial_putc(data->fd, buf[i]);
  }

  return true;
}

/* end of hw10-main.c */
